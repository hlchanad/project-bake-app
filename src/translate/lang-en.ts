export const LANG_EN_NAME = 'en';

export const LANG_EN_SUFFIX = '';

export const LANG_EN_TRANSLATION = {
  // general
  'recipes': 'Recipes',
  'shoppingList': 'Shopping List',
  'favoriteRecipes': 'Favorite',
  'settings': 'Settings',
  'okay': 'Okay',
  'cancel': 'Cancel',

  // recipe page
  'size': 'Size',

  // recipe page overview segment
  'overview': 'Overview',

  // recipe page ingredient segment
  'ingredients': 'Ingredients',
  'addAllToShoppingList': 'Add all ingredients to shopping list',
  'bulkBuyToggle': 'Bulk buy',
  'bulkBuy': 'Buy!',
  'addedToShoppingList': 'Added to Shopping List',

  // recipe page step segment
  'steps': 'Steps',
  'finishRecipe': 'Congratulation! You have finished the recipe!',
  'finish': 'Finish',
  'timesup': 'Time\'s up!',

  // recipe page photos segment
  'photos': 'Photos',

  // shopping list page
  'buy': 'Buy'
};
