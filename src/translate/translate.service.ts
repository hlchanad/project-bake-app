import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

import { SUPPORTED_LANGUAGES, DICTIONARY } from './translation';
import { DEFAULT_LANG } from './config';
import { LANG_EN_NAME, LANG_EN_SUFFIX } from './lang-en';
import { LANG_ZH_NAME, LANG_ZH_SUFFIX } from './lang-zh';

@Injectable()
export class TranslateService {

  currentLang: string;
  dictionary: any;
  supportedLanguages: { display: string, code: string }[];

  constructor(private storage: Storage) {
    this.currentLang = DEFAULT_LANG;
    this.dictionary = DICTIONARY;
    this.supportedLanguages = SUPPORTED_LANGUAGES;

    this.storage.get('preferredLang')
      .then((lang: string) => this.currentLang = lang ? lang : this.currentLang)
      .catch();
  }

  getCurrentLangSuffix() {
    switch (this.currentLang) {
      case LANG_EN_NAME: return LANG_EN_SUFFIX;
      case LANG_ZH_NAME: return LANG_ZH_SUFFIX;
    }
  }

  getCurrentLanguage() {
    return this.supportedLanguages.find(
      (language) => {
        return language.code == this.currentLang;
      }
    );
  }

  getSupportedLanguages() {
    return this.supportedLanguages;
  }

  use(lang: string) {
    if (!this.isLangSupported(lang)) {
      return ; // ignore if not allowed lang choice
    }
    this.currentLang = lang;
    this.storage.set('preferredLang', lang)
      .then()
      .catch();
  }

  translate(key: string) {
    if (this.dictionary[this.currentLang][key]) {
      return this.dictionary[this.currentLang][key];
    }
    else {
      return key;
    }
  }

  private isLangSupported(lang: string) {
    for(let i=0; i<this.supportedLanguages.length; i++) {
      if (this.supportedLanguages[i].code == lang) {
        return true;
      }
    }
    return false;
  }
}
