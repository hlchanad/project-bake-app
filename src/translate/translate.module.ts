import { NgModule } from '@angular/core';

import { TranslatePipe } from './translate.pipe';
import { TranslateContentPipe } from './translate-content.pipe';

@NgModule({
  declarations: [ TranslatePipe, TranslateContentPipe ],
  exports: [ TranslatePipe, TranslateContentPipe ]
})
export class TranslateModule {}
