import { Injectable, Pipe, PipeTransform } from '@angular/core';

import { TranslateService } from './translate.service';

@Injectable()
@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translate: TranslateService) {}

  transform(key: string) {
    return this.translate.translate(key);
  }
}
