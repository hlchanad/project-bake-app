export const LANG_ZH_NAME = 'zh';

export const LANG_ZH_SUFFIX = '_zh';

export const LANG_ZH_TRANSLATION = {
  // general
  'recipes': '食譜',
  'shoppingList': '購物清單',
  'favoriteRecipes': '喜愛',
  'settings': '設定',
  'okay': '好',
  'cancel': '取消',

  // recipe page
  'size': '份量',

  // recipe page overview segment
  'overview': '概覽',

  // recipe page ingredient segment
  'ingredients': '材料',
  'addAllToShoppingList': '把所有材料加到購物清單',
  'bulkBuyToggle': '要不一起買?',
  'bulkBuy': '買!',
  'addedToShoppingList': '已加到購物清單',

  // recipe page step segment
  'steps': '做法',
  'finishRecipe': '恭喜你已完成這份食譜!',
  'finish': '完成',
  'timesup': 'Time\'s up!',

  // recipe page photos segment
  'photos': '圖片',

  // shopping list page
  'buy': '買'

};
