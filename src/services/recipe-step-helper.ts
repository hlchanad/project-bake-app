import { Recipe } from '../models/recipe';
import { EventEmitter, Injectable } from '@angular/core';

import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications';

import { Timer } from '../shared/timer';
import { StepsComponent } from '../pages/recipe/components/steps/steps';
import { TranslateService } from '../translate/translate.service';
import { DevService } from './dev';

export const STATUS_PLAYING = 1;
export const STATUS_PAUSING = 2;

export interface CurrentStep {
  part: number;
  step: number;
}

@Injectable()
export class RecipeStepHelperService {

  stepsComponent: StepsComponent;
  recipe: Recipe;
  status = STATUS_PAUSING; // or 'pausing'
  currentStep: CurrentStep;

  timer: Timer;
  timeInSeconds: number;

  localPushId: any;

  stepChanges    = new EventEmitter<{ currentStep: CurrentStep, isClickStepDirectly: boolean }>();
  finishedRecipe = new EventEmitter<void>();
  finishedTimer  = new EventEmitter<void>();

  constructor(private localNotifications: LocalNotifications,
              private translateService: TranslateService,
              private devService: DevService) {}

  setRecipe(recipe: Recipe, stepsComponent: StepsComponent) {
    // reset/ init
    this.currentStep = null;
    this.status = STATUS_PAUSING;
    this.timer = null;
    this.timeInSeconds = null;

    this.recipe = recipe;
    this.stepsComponent = stepsComponent;
  }

  startFromStep(partIndex: number, stepIndex: number, isClickStepDirectly = true) {
    this.currentStep = { part: partIndex, step: stepIndex };
    this.status = STATUS_PLAYING;
    this.changedCurrentStep(isClickStepDirectly);
  }

  next() {
    if (!this.currentStep) {
      this.currentStep = { part: 0, step: 0 };
      this.changedCurrentStep();
    }
    else if (this.recipe.parts[this.currentStep.part].steps[this.currentStep.step+1]) {
      this.currentStep.step++;
      this.changedCurrentStep();
    }
    else if (this.recipe.parts[this.currentStep.part+1]) {
      this.currentStep.part++;
      this.currentStep.step = 0;
      this.changedCurrentStep();
    }
    else {
      this.stepsComponent.onFinish();
    }
  }

  previous() {
    if (this.currentStep.step-1 >= 0) {
      this.currentStep.step--;
      this.changedCurrentStep();
    }
    else if (this.currentStep.part-1 >= 0) {
      this.currentStep.part--;
      this.currentStep.step = this.recipe.parts[this.currentStep.part].steps.length-1;
      this.changedCurrentStep();
    }
  }

  stop() {
    this.currentStep = null;
    this.status = STATUS_PAUSING;

    if (this.timer && this.timer.isOngoing()) {
      this.timer.stop();
      // this.clearLocalPush();
    }
  }

  finish() {
    this.stop();
    this.finishedRecipe.emit();
  }

  pause() {
    this.timer.pause();
    // this.clearLocalPush()
  }

  resume() {
    this.timer.resume();
    // this.scheduleLocalPush(Math.ceil(this.timer.getRemaining() / 1000));
  }

  private changedCurrentStep(isClickStepDirectly = false) {

    if (this.timer && this.timer.isOngoing()) {
      this.timer.stop();
      // this.clearLocalPush();
    }

    const step = this.recipe.parts[this.currentStep.part].steps[this.currentStep.step];
    if (step.autoNextStep) {
      this.timeInSeconds = step.duration;

      this.timer = new Timer(() => {
        // this.next();
        this.timeInSeconds = 0;
        this.finishedTimer.emit();
      }, (this.timeInSeconds) * 1000);

      // this.scheduleLocalPush(this.timeInSeconds);
    }

    this.stepChanges.emit({ currentStep: this.currentStep, isClickStepDirectly: isClickStepDirectly });
  }

  getStatus() {
    return this.status;
  }

  toggleStatus() {
    this.status = this.status == STATUS_PLAYING ? STATUS_PAUSING : STATUS_PLAYING;
  }

  scheduleLocalPush(timeInSeconds: number) {
    this.localPushId = 1;

    this.localNotifications.schedule(<ILocalNotification>{
      // id: this.localPushId,
      text: this.translateService.translate('timesup'),
      at: new Date((new Date()).setSeconds((new Date()).getSeconds() + timeInSeconds)),
      // data: { recipeId: this.recipe._id }
    });
  }

  clearLocalPush() {
    this.devService.addLog('clearLocalPush() start', '');

    this.localNotifications.on('cancelall', () => this.devService.addLog('onCancelAll', ''));
    this.localNotifications.on('schedule', () => this.devService.addLog('onSchedule', ''));
    this.localNotifications.on('trigger', () => this.devService.addLog('onTrigger', ''));
    this.localNotifications.on('click', () => this.devService.addLog('onClick', ''));

    this.localNotifications.getAllIds()
      .then((data) => this.devService.addLog('getAllIds() success', data))
      .catch((error) => this.devService.addLog('getAllIds() fail', error));

    this.localNotifications.getTriggeredIds()
      .then((data) => this.devService.addLog('getTriggeredIds() success', data))
      .catch((error) => this.devService.addLog('getTriggeredIds() fail', error));

    this.localNotifications.getScheduledIds()
      .then((data) => this.devService.addLog('getScheduledIds() success', data))
      .catch((error) => this.devService.addLog('getScheduledIds() fail', error));

    this.localNotifications.isPresent(0)
      .then((data) => this.devService.addLog('isPresent() success', data))
      .catch((error) => this.devService.addLog('isPresent() fail', error));

    this.localNotifications.isTriggered(0)
      .then((data) => this.devService.addLog('isTriggered() success', data))
      .catch((error) => this.devService.addLog('isTriggered() fail', error));

    this.localNotifications.isScheduled(0)
      .then((data) => this.devService.addLog('isScheduled() success', data))
      .catch((error) => this.devService.addLog('isScheduled() fail', error));

    this.localNotifications.cancelAll()
      .then((data) => this.devService.addLog('cancelAll() success', data))
      .catch((error) => this.devService.addLog('cancelAll() fail', error));
    this.localPushId = null;
  }

}
