import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import * as HttpStatus from 'http-status-codes';

import { HOST_URL } from '../config/config';
import { Recipe } from '../models/recipe';

@Injectable()
export class ApiService {

  constructor(private http: Http,
              private storage: Storage) {}

  getRecipesFromServer() {
    return new Promise<Recipe[]>((resolve, reject) => {

      this.setIfModifiedSince('recipe')
        .then((requestOptions: RequestOptions) => {

          this.http.get(HOST_URL + '/recipe', requestOptions)
            .subscribe(
              (response: Response) => {
                this.updateApiUt('recipe', response.headers.get('last-modified'))
                  .then()
                  .catch();
                resolve(response.json().data.recipes);
              },
              (error) => {
                if (error.status == HttpStatus.NOT_MODIFIED) {
                  resolve([]);
                }
                else {
                  reject(error);
                }
              }
            )
        })
        .catch();
    });
  }

  regToken(platform: string, uuid: string) {
    return new Promise<string>((resolve, reject) => {
      this.http.post(HOST_URL + '/device/regToken', { deviceType: platform, deviceToken: uuid })
        .subscribe(
          (response: Response) => resolve(),
          (error) => reject(error));
    });
  }

  private setIfModifiedSince(api: string) {

    return new Promise<RequestOptions>((resolve, reject) => {
      this.storage.get('apiUt')
        .then((apiUt) => {
          if (apiUt == null) { // first time open app/ call this api
            resolve(new RequestOptions());
          }
          else {
            resolve(new RequestOptions({
              headers: new Headers({ 'if-modified-since': apiUt[api] })
            }));
          }
        })
        .catch(
          (error) => {
            reject(error);
          });
    });
  }

  private updateApiUt(api: string, lastModified: string) {

    return new Promise<void>((resolve, reject) => {
      this.storage.get('apiUt')
        .then((apiUt) => {

          if (apiUt == null) { // first time open app/ call this api
            apiUt = {};
          }
          apiUt[api] = lastModified;

          this.storage.set('apiUt', apiUt)
            .then(() => {
              resolve();
            })
            .catch((error) => {
              reject(error);
            });
        })
        .catch(
          (error) => {
            reject(error);
          });
    });
  }
}
