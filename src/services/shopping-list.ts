import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { ShoppingListIngredient, IngredientStatus } from '../models/shopping-list-ingredient';
import { RecipeWiseShoppingListIngredient } from '../models/recipe-wise-shopping-list-ingredient';

@Injectable()
export class ShoppingListService {

  private ingredients: ShoppingListIngredient[] = [];
  private recipeWiseIngredients: RecipeWiseShoppingListIngredient[] = [];

  constructor(private storage: Storage){

    this.storage.get('recipeWiseIngredients')
      .then((ingredients: RecipeWiseShoppingListIngredient[]) => {
        this.recipeWiseIngredients = ingredients ? ingredients : [];
      });
  }

  getIngredientsFromStorage() {
    return this.storage.get('ingredients')
      .then((ingredients: ShoppingListIngredient[]) => {
        this.ingredients = ingredients ? ingredients : [];
        return this.ingredients;
      });
  }

  getIngredients() {
    // return this.ingredients.slice();
    return this.ingredients.filter((ingredient: ShoppingListIngredient) => {
      return ingredient.status !== IngredientStatus.Bought;
    });
  }

  getRecipeWiseIngredients() {
    return this.recipeWiseIngredients.slice();
  }

  addIngredient(ingredient: ShoppingListIngredient) {
    // handle this.ingredients
    this.ingredients.push(ingredient);

    // handle this.recipeWiseIngredients
    if (ingredient.recipeRef != null) {

      const recipeId = ingredient.recipeRef.recipeId,
            partIndex = ingredient.recipeRef.partIndex,
            ingredientIndex = ingredient.recipeRef.ingredientIndex;

      let index = this.recipeWiseIngredients.findIndex((ingredientEl: RecipeWiseShoppingListIngredient) => {
        return ingredientEl.recipeId == recipeId;
      });

      if (index < 0) {
        this.recipeWiseIngredients.push({ recipeId: recipeId, parts: {} });
        index = this.recipeWiseIngredients.length - 1;
      }

      if (!this.recipeWiseIngredients[index].parts[partIndex]) {
        this.recipeWiseIngredients[index].parts[partIndex] = {};
      }

      this.recipeWiseIngredients[index].parts[partIndex][ingredientIndex] = ingredient;
    }

    this.updateIngredientsToStorage();
  }

  boughtIngredient(ingredient: ShoppingListIngredient) {
    // handle this.ingredients
    const index = this.ingredients.findIndex(
      (ingredientElement: ShoppingListIngredient) => {
        if (ingredient.recipeRef && ingredientElement.recipeRef) {
          // handle case that bought from recipe page ( + some in shopping list page )
          return ingredient.recipeRef.recipeId == ingredientElement.recipeRef.recipeId
            && ingredient.recipeRef.partIndex == ingredientElement.recipeRef.partIndex
            && ingredient.recipeRef.ingredientIndex == ingredientElement.recipeRef.ingredientIndex;
        }
        else {
          // handle from shopping list page
          return ingredientElement._id == ingredient._id;
        }
      }
    );
    this.ingredients[index].status = IngredientStatus.Bought;

    // handle this.recipeWiseIngredients
    if (ingredient.recipeRef != null) {
      const partIndex = ingredient.recipeRef.partIndex,
            ingredientIndex = ingredient.recipeRef.ingredientIndex;

      const index2 = this.recipeWiseIngredients.findIndex(
        (ingredientElement: RecipeWiseShoppingListIngredient) => {
          return ingredientElement.recipeId == ingredient.recipeRef.recipeId;
        }
      );

      this.recipeWiseIngredients[index2].parts[partIndex][ingredientIndex].status = IngredientStatus.Bought;
    }

    this.updateIngredientsToStorage();
  }

  finishRecipe(recipeId: string) {
    const index = this.recipeWiseIngredients.findIndex(
      (ingredient: RecipeWiseShoppingListIngredient) => {
        return ingredient.recipeId == recipeId;
      }
    );

    if (index >= 0) {
      this.recipeWiseIngredients.splice(index, 1);
    }

    while(true) {
      const index = this.ingredients.findIndex(
        (ingredient: ShoppingListIngredient) => {
          return ingredient.recipeRef && ingredient.recipeRef.recipeId == recipeId;
        }
      );

      if (index < 0) break;
      this.ingredients.splice(index, 1);
    }

    this.updateIngredientsToStorage();
  }

  isIngredientBought(recipeId: string, partIndex: number, ingredientIndex: number) {
    const index = this.recipeWiseIngredients.findIndex(
      (ingredient: RecipeWiseShoppingListIngredient) => {
        return ingredient.recipeId == recipeId;
      }
    );

    if (index < 0) { return false; }

    return this.recipeWiseIngredients[index].parts[partIndex][ingredientIndex].status == IngredientStatus.Bought;
  }

  updateIngredientsToStorage() {
    this.storage.set('ingredients', this.ingredients);
    this.storage.set('recipeWiseIngredients', this.recipeWiseIngredients);
  }

  canRecipeAddedToShoppingList(recipeId: string) {
    const index = this.recipeWiseIngredients.findIndex(
      (ingredient: RecipeWiseShoppingListIngredient) => {
        return ingredient.recipeId == recipeId;
      }
    );
    return index < 0;
  }

  isAllIngredientsAreBoughtFromRecipe(recipeId: string): boolean {
    // try to find any "Not Bought" item
    for(let i = 0; i < this.ingredients.length; i++) {
      const ingredient: ShoppingListIngredient = this.ingredients[i];

      if (ingredient.recipeRef
        && ingredient.recipeRef.recipeId === recipeId
        && ingredient.status === IngredientStatus.Active) {

        return false;
      }
    }

    return true;
  }
}

