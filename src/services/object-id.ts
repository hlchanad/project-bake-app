import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

interface ObjectId {
  [type: string]: number;
}

@Injectable()
export class ObjectIdService {

  objectId: ObjectId;

  constructor(private storage: Storage) {

    this.getObjectIdFromStorage()
      .then((objectId: ObjectId) => this.objectId = objectId)
      .catch();
  }

  getObjectId(type: string) {

    if (!this.objectId[type]) {
      this.objectId[type] = 1;
    }

    this.objectId[type]++;

    this.storeObjectIdToStorage(this.objectId).then().catch();

    return this.objectId[type]-1;
  }


  getObjectIdFromStorage() {
    return new Promise<ObjectId>((resolve, reject) => {
      this.storage.get('object-id')
        .then((objectId: ObjectId) => {
          objectId = objectId != null ? objectId : {};
          resolve(objectId);
        })
        .catch(err => reject(err));
    });
  }

  storeObjectIdToStorage(objectId: ObjectId) {
    return new Promise<void>((resolve, reject) => {
      this.storage.set('object-id', objectId)
        .then(() => resolve())
        .catch(err => reject(err));
    });
  }
}
