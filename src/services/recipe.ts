import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Recipe, RecipeStatus } from '../models/recipe';
import { ApiService } from './api';
import { FavoriteRecipe } from '../models/favorite-recipe';

@Injectable()
export class RecipeService {

  private recipes: Recipe[] = [];
  private favoriteRecipes: FavoriteRecipe[] = [];

  constructor(private apiService: ApiService,
              private storage: Storage) {

  }

  getRecipes() {
    return new Promise<Recipe[]>((resolve, reject) => {
      if (this.recipes.length > 0) {
        resolve(this.recipes.slice());
      }
      else {
        // see if storage got some recipes
        this.getRecipesFromStorage()
          .then((recipes: Recipe[]) => {

            this.recipes = recipes;
            this.recipes.sort(this.sortByUpdatedAtDesc);
            this.recipes = this.filterByIsShow(this.recipes);

            resolve(this.recipes);
          })
          .catch((error) => {
            reject(error);
          });
      }
    });
  }

  getRecipesFromServer() {
    return new Promise<Recipe[]>((resolve, reject) => {
      this.apiService.getRecipesFromServer()
        .then((recipes: Recipe[]) => {

          this.updateRecipes(recipes);

          this.recipes.sort(this.sortByUpdatedAtDesc);
          this.recipes = this.filterByIsShow(this.recipes);

          this.storeRecipesToStorage(this.recipes)
            .then(() => {
              resolve(this.recipes);
            })
            .catch(error => reject(error));
        })
        .catch(error => reject(error));
    });
  }

  getRecipeById(recipeId: string) {
    return this.recipes.find((recipe: Recipe) => recipeId === recipe._id);
  }

  getRecipeName(recipeId: string) {
    const recipe = this.getRecipeById(recipeId);
    return {
      title: recipe.title,
      title_zh: recipe.title_zh
    };
  }

  // --- Favorite Recipes -------------------------------------------------------

  isRecipeFavorited(recipeId: string) {
    const index = this.favoriteRecipes.findIndex((recipe: FavoriteRecipe) => recipe.recipeId === recipeId);
    return index >= 0;
  }

  favoriteRecipe(recipeId: string) {
    if (this.getRecipeById(recipeId)) {
      this.favoriteRecipes.push({ recipeId: recipeId });
      this.storeFavoriteRecipesToStorage();
    }
  }

  removeFavoriteRecipe(recipeId: string) {
    const index = this.favoriteRecipes.findIndex((recipe: FavoriteRecipe) => recipe.recipeId === recipeId);
    if (index >= 0) {
      this.favoriteRecipes.splice(index, 1);
      this.storeFavoriteRecipesToStorage();
    }
  }

  getFavoriteRecipes(): Promise<Recipe[]>{
    return new Promise<Recipe[]>((resolve, reject) => {
      if (this.favoriteRecipes.length > 0) {
        resolve(this.favoriteRecipes.slice()
          .map((favoriteRecipe: FavoriteRecipe) => this.getRecipeById(favoriteRecipe.recipeId)));
      }
      else {
        // see if storage got some recipes
        this.getFavoriteRecipesFromStorage()
          .then((favoriteRecipes: FavoriteRecipe[]) => {
            this.favoriteRecipes = favoriteRecipes;
            resolve(this.filterByIsShow(this.favoriteRecipes
              .map((favoriteRecipe: FavoriteRecipe) => this.getRecipeById(favoriteRecipe.recipeId))));
          })
          .catch((error) => reject(error));
      }
    });
  }

  // ----------------------------------------------------------------------------

  private updateRecipes(recipes: Recipe[]) {
    for(let i=0; i<recipes.length; i++) {
      const index = this.recipes.findIndex((recipe: Recipe) => {
        return recipe._id == recipes[i]._id;
      });

      if (index >= 0) {
        this.recipes[index] = recipes[i];
      }
      else {
        this.recipes.push(recipes[i]);
      }

    }
  }

  private getFavoriteRecipesFromStorage(): Promise<FavoriteRecipe[]> {
    return new Promise<FavoriteRecipe[]>((resolve, reject) => {
      this.storage.get('favoriteRecipes')
        .then((favoriteRecipes: FavoriteRecipe[]) => resolve(favoriteRecipes || []))
        .catch(error => reject(error));
    });
  }

  private storeFavoriteRecipesToStorage(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.storage.set('favoriteRecipes', this.favoriteRecipes)
        .then(() => resolve())
        .catch((error) => reject(error));
    });
  }

  private getRecipesFromStorage(): Promise<Recipe[]> {
    return new Promise<Recipe[]>((resolve, reject) => {
      this.storage.get('recipes')
        .then((recipes: Recipe[]) => {
          if (recipes == null)
            resolve([]);
          else
            resolve(recipes);
        })
        .catch(error => reject(error));
    });
  }

  private storeRecipesToStorage(recipes: Recipe[]) {
    return new Promise<void>((resolve, reject) => {
      this.storage.set('recipes', recipes)
        .then(() => resolve())
        .catch(error => reject(error));
    });
  }

  private sortByUpdatedAtDesc(a: Recipe, b: Recipe) {
    const aUpdatedAt = new Date(a.updatedAt);
    const bUpdatedAt = new Date(b.updatedAt);

    if (aUpdatedAt < bUpdatedAt)      return 1;
    else if (aUpdatedAt > bUpdatedAt) return -1;
    else                              return 0;
  }

  private filterByIsShow(recipes: Recipe[]): Recipe[] {
    return recipes.filter((recipe: Recipe) => recipe.status == RecipeStatus.ACTIVE);
  }


}
