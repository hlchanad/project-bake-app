
export class DevService {

  logs: string[] = [];

  addLog(tag: string, obj){
    this.logs.push(tag + JSON.stringify(obj));
    console.log(tag, obj);
  }

  getLogs() {
    return this.logs.slice();
  }

  discardLogs() {
    this.logs = [];
  }
}
