import { NgModule } from '@angular/core';

import { SquareDirective } from './square.directive';

@NgModule({
  declarations: [ SquareDirective ],
  exports: [ SquareDirective ]
})
export class SharedModule {}
