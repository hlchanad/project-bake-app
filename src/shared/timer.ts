export class Timer {

  private timeoutId: any;
  private start: number;
  private finished = false;

  constructor(private callback: Function, private remaining: number){
    this.resume();
  }

  stop() {
    clearTimeout(this.timeoutId);
    this.timeoutId = null;
    this.finished = true;
  }

  pause() {
    clearTimeout(this.timeoutId);
    this.timeoutId = null;
    this.remaining = this.remaining - ((new Date()).getTime() - this.start);
  }

  resume() {
    this.start = new Date().getTime();
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(() => {
      this.callback();
      this.finished = true;
    }, this.remaining);
  }

  isOngoing(){
    return !this.finished;
  }

  getRemaining() {
    return this.remaining;
  }
}
