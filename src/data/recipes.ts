export default [
  {
    "title": "Chocolate Meringue Pie",
    "desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec tristique ipsum. Sed pellentesque, justo vitae tristique rutrum, augue est consequat nulla, a auctor libero lectus a tellus. Fusce faucibus ac diam bibendum auctor. Nunc at bibendum enim. Fusce interdum nulla sed felis vestibulum maximus. Proin id accumsan elit, tempor porta nulla. Proin porta, leo eu imperdiet dictum, enim dolor facilisis nulla, id sagittis nunc quam sed nulla. Nullam leo risus, tincidunt vitae dolor non, suscipit ullamcorper sem. Proin vitae suscipit erat. Phasellus eu posuere nibh. Aenean quis leo a metus ultrices ornare a vitae nisl. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "8個2.5\"撻/ 2個6\"pan",
    "images": [
      {
        "imageUrl": "http://via.placeholder.com/1000x1000"
      }, {
        "imageUrl": "http://via.placeholder.com/20x20"
      }, {
        "imageUrl": "http://via.placeholder.com/30x30"
      }, {
        "imageUrl": "http://via.placeholder.com/40x40"
      }, {
        "imageUrl": "http://via.placeholder.com/50x50"
      }
    ],
    "parts": [
      {
        "title": "餅底",
        "desc": "more than enough",
        "ingredients": [
          {
            "name": "中筋麵粉",
            "quantity": "200",
            "unit": "g"
          }, {
            "name": "無糖可可粉",
            "quantity": "30",
            "unit": "g"
          }, {
            "name": "無鹽牛油",
            "quantity": "120",
            "unit": "g"
          }, {
            "name": "砂糖",
            "quantity": "30",
            "unit": "g"
          }, {
            "name": "蛋黃",
            "quantity": "2",
            "unit": "pcs"
          }, {
            "name": "水",
            "quantity": "1-2",
            "unit": "湯匙"
          }, {
            "name": "鹽",
            "quantity": "1/8",
            "unit": "茶匙"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 5,
            "line": "搞勻麵粉可可粉之後落牛油鹽至粗粒"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加蛋黃糖霜同1湯匙水"
          }, {
            "autoNextStep": true,
            "duration": 5,
            "line": "搞到一舊乾淨嘅dough就ok (加水if needed)"
          }, {
            "autoNextStep": true,
            "duration": 10,
            "line": "保鮮紙包住雪30分鐘"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "grease下個pan放個麵糰落去 (大約5mm厚)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "用叉吉下個皮 等啲空氣喺焗嗰時走到"
          }, {
            "autoNextStep": true,
            "duration": 900,
            "line": "180度焗15分鐘左右"
          }
        ]
      }, {
        "title": "餡",
        "desc": "",
        "ingredients": [
          {
            "name": "黑朱古力",
            "quantity": "150",
            "unit": "g"
          }, {
            "name": "動物性忌廉",
            "quantity": "60",
            "unit": "mL"
          }, {
            "name": "無鹽牛油",
            "quantity": "60",
            "unit": "g"
          }, {
            "name": "糖霜",
            "quantity": "50",
            "unit": "g"
          }, {
            "name": "蛋黃",
            "quantity": "4",
            "unit": "pcs"
          }, {
            "name": "雲呢拿油",
            "quantity": "1",
            "unit": "tbsp"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "隔水加熱朱古力忌廉同雲呢拿油"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "搞起牛油同糖"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "再加蛋黃 朱古力mixture 同杏仁粉"
          }, {
            "autoNextStep": true,
            "duration": 600,
            "line": "倒落去個撻到焗10分鐘"
          }
        ]
      }, {
        "title": "Meringue",
        "desc": "",
        "ingredients": [
          {
            "name": "糖霜",
            "quantity": "100",
            "unit": "g"
          }, {
            "name": "蛋白",
            "quantity": "2",
            "unit": "pcs"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "打蛋白至soft peak (濕)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "再加糖打到glossy peak (乾)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "放落個tart到特登整好多個尖出黎"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "焗5分鐘左右 直至啲peak金黃色"
          }
        ]
      }
    ]
  },
  {
    "title": "朱古力心太軟",
    "desc": "Super tasty",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "2人份量",
    "images": [],
    "parts": [
      {
        "title": "心太軟",
        "desc": "a bit more than enough though",
        "ingredients": [
          {
            "name": "黑朱古力",
            "quantity": "80",
            "unit": "g"
          }, {
            "name": "無鹽牛油",
            "quantity": "80",
            "unit": "g"
          }, {
            "name": "蛋",
            "quantity": "2",
            "unit": "pcs"
          }, {
            "name": "砂糖",
            "quantity": "40",
            "unit": "g"
          }, {
            "name": "低筋麵粉",
            "quantity": "20",
            "unit": "g"
          }

        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "溶左d朱古力同牛油"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "將蛋同糖攪攪攪, 直至糖溶"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "慢慢將朱古力倒入蛋液到"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加入已過篩麵粉"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "個杯要用牛油搽, 再灑面粉, 以防脫膜失敗"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "用220度焗6-7分鐘"
          }
        ]
      }
    ]
  },
  {
    "title": "Brownie",
    "desc": "Super tasty",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "16cm² 正方形pan",
    "images": [],
    "parts": [
      {
        "title": "Brownie",
        "desc": "",
        "ingredients": [
          {
            "name": "黑朱古力",
            "quantity": "200",
            "unit": "g"
          }, {
            "name": "無鹽牛油",
            "quantity": "100",
            "unit": "g"
          }, {
            "name": "蛋",
            "quantity": "2",
            "unit": "pcs"
          }, {
            "name": "蛋黃",
            "quantity": "1",
            "unit": "pcs"
          }, {
            "name": "砂糖",
            "quantity": "60",
            "unit": "g"
          }, {
            "name": "低筋麵粉",
            "quantity": "50",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "溶左d朱古力同牛油"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "將蛋同糖攪攪攪, 直至糖溶"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "慢慢將朱古力倒入蛋液到"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加入麵粉 (可以不過篩)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "用180度焗15分鐘左右 (如果想焗多陣/唔夠熟要落錫紙)"
          }
        ]
      }
    ]
  },
  {
    "title": "Alfajores with dulce de leche",
    "desc": "Super tasty",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "~15塊(?)",
    "images": [],
    "parts": [
      {
        "title": "餅",
        "desc": "",
        "ingredients": [
          {
            "name": "無鹽牛油",
            "quantity": "250",
            "unit": "g"
          }, {
            "name": "糖霜 (都已經有啲甜)",
            "quantity": "75",
            "unit": "g"
          }, {
            "name": "粟粉",
            "quantity": "225",
            "unit": "g"
          }, {
            "name": "低筋麵粉",
            "quantity": "150",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "溶左啲牛油"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "所有粉過哂篩"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "倒牛油落去搞勻成麵糰"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "放入雪櫃雪30分鐘"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "用~5cm圓形曲奇模切~1cm曲奇出黎 (豐儉由人)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "180度焗10分鐘左右至金黃 或者如果想白雪雪就留意時間"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "焗完放喺鐵架到吹凍至拎得起唔爛"
          }
        ]
      }, {
        "title": "餡",
        "desc": "",
        "ingredients": [
          {
            "name": "煉奶",
            "quantity": "200",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "成罐煉奶唔好開蓋放入煲"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加水至高過煉奶罐 (唔夠水會爆)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "煲水煲到滾就轉細火"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "煲三個鐘"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "放涼至唔熱"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "將餡夾喺兩塊餅中間"
          }
        ]
      }
    ]
  },
  {
    "title": "牛油曲奇",
    "desc": "Super tasty",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "豐儉由人啦",
    "images": [],
    "parts": [
      {
        "title": "牛油曲奇",
        "desc": "",
        "ingredients": [
          {
            "name": "無鹽牛油",
            "quantity": "200",
            "unit": "g"
          }, {
            "name": "蛋",
            "quantity": "1",
            "unit": "pcs"
          }, {
            "name": "糖霜",
            "quantity": "100",
            "unit": "g"
          }, {
            "name": "低筋麵粉",
            "quantity": "250",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "攪牛油同糖霜直至軟滑"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加入蛋並攪勻"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "加入麵粉 (不用過篩)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "啫啫啫~"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "放雪櫃20分鐘 (我都仲未理解幾時會塌幾時唔會)"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "180度焗10分鐘(?)"
          }
        ]
      }
    ]
  },
  {
    "title": "抹茶卷蛋",
    "desc": "Super tasty",
    "thumbnail": {
      "imageUrl": "http://via.placeholder.com/300x300"
    },
    "size": "~A4 size 長方形pan => 1大條",
    "images": [],
    "parts": [
      {
        "title": "蛋黃糊",
        "desc": "",
        "ingredients": [
          {
            "name": "蛋黃",
            "quantity": "3",
            "unit": "pcs"
          }, {
            "name": "砂糖",
            "quantity": "15",
            "unit": "g"
          }, {
            "name": "奶",
            "quantity": "適量",
            "unit": ""
          }, {
            "name": "低粉",
            "quantity": "53",
            "unit": "g"
          }, {
            "name": "抹茶粉 ",
            "quantity": "7",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "打起蛋同糖"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "過篩落低粉同抹茶粉"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "途中如果太稠就落啲奶"
          }
        ]
      }, {
        "title": "蛋白霜",
        "desc": "",
        "ingredients": [
          {
            "name": "蛋白",
            "quantity": "3",
            "unit": "pcs"
          }, {
            "name": "砂糖",
            "quantity": "40",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "打起至乾性發泡"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "分幾次加蛋白霜入蛋黃糊"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "預熱170度"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "大約焗10分鐘"
          }
        ]
      }, {
        "title": "抹茶忌廉",
        "desc": "應該要多啲cream, 依家有少少唔夠滿",
        "ingredients": [
          {
            "name": "淡忌廉",
            "quantity": "250",
            "unit": "ml"
          }, {
            "name": "抹茶粉",
            "quantity": "5",
            "unit": "g"
          }, {
            "name": "砂糖",
            "quantity": "15",
            "unit": "g"
          }
        ],
        "steps": [
          {
            "autoNextStep": false,
            "duration": 0,
            "line": "喺蛋糕上用刀切不到底的切痕, 方便捲而不爛"
          }, {
            "autoNextStep": false,
            "duration": 0,
            "line": "捲完放入雪櫃一晚"
          }
        ]
      }
    ]
  }
];
