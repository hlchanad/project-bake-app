import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform{

  transform(value: string, limitInput: string, trailInput: string) : string {

    let limit = limitInput != null && limitInput != "" ? parseInt(limitInput, 10) : 10;
    let trail = trailInput != null && trailInput != "" ? trailInput : '...';

    return value.length > limit ? value.substring(0, limit) + trail : value;
  }
}
