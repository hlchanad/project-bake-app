import { Ingredient } from './ingredient';
import { RecipeRef } from './recipe-ref';

export enum IngredientStatus {
  Active = 1,
  Bought = 2
}

export interface ShoppingListIngredient extends Ingredient {

  _id: number;

  name: string;
  quantity: string;
  unit: string;

  name_zh: string;
  quantity_zh: string;
  unit_zh: string;

  status: IngredientStatus;
  recipeRef: RecipeRef;
}
