export interface RecipeRef {
  recipeId: string;
  partIndex: number;
  ingredientIndex: number;
}
