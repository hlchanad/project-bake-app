import { Ingredient } from './ingredient';
import { Step } from './step';

export interface Part {
  title: string;
  desc: string;
  ingredients: Ingredient[];
  steps: Step[];
}
