import { ShoppingListIngredient } from './shopping-list-ingredient';

export interface RecipeWiseShoppingListIngredient {

  recipeId: string;
  parts: {
    [index: number]: {
      [index: number]: ShoppingListIngredient;
    };
  };
}
