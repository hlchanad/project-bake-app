import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device';

import { MyApp } from './app.component';

import { TranslateModule } from '../translate/translate.module';
import { TabsPageModule } from '../pages/tabs/tabs.module';
import { RecipesPageModule } from '../pages/recipes/recipes.module';
import { RecipePageModule } from '../pages/recipe/recipe.module';
import { ShoppingListPageModule } from '../pages/shopping-list/shopping-list.module';
import { FavoriteRecipesPageModule } from '../pages/favorite-recipes/favorite-recipes.module';
import { SettingsPageModule } from '../pages/settings/settings.module';
import { DevPageModule } from '../pages/dev/dev.module';

import { TranslateService } from '../translate/translate.service';
import { ShoppingListService } from '../services/shopping-list';
import { ApiService } from '../services/api';
import { DevService } from '../services/dev';
import { ObjectIdService } from '../services/object-id';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


@NgModule({
  declarations: [ MyApp ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { backButtonText: ' ' }),
    HttpModule,
    IonicStorageModule.forRoot(),
    TranslateModule,
    TabsPageModule,
    RecipesPageModule,
    RecipePageModule,
    ShoppingListPageModule,
    FavoriteRecipesPageModule,
    SettingsPageModule,
    DevPageModule
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [ MyApp ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Device,
    GoogleAnalytics,
    ShoppingListService,
    TranslateService,
    ApiService,
    DevService,
    ObjectIdService
  ]
})
export class AppModule {}
