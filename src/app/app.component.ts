import { Component } from '@angular/core';

import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { ApiService } from '../services/api';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { DevService } from '../services/dev';
import { APP_VERSION, DEV_MODE } from '../config/config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp{
  rootPage:any = TabsPage;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private storage: Storage,
              private device: Device,
              private ga: GoogleAnalytics,
              private apiService: ApiService,
              private devService: DevService) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.regToken();

      if (!DEV_MODE) {
        this.initGoogleAnalytics();
      }
    });
  }

  initGoogleAnalytics() {
    const platform = this.device.platform || 'Web',
          uuid = this.device.uuid || 'faked_uuid';

    this.ga.startTrackerWithId('UA-105686596-1')
      .then(() => {
        this.devService.addLog('GA is ready', '');

        this.ga.setAppVersion(APP_VERSION)
          .then(() => this.devService.addLog('GA setAppVersion success', ''))
          .catch(error => this.devService.addLog('GA setAppVersion fail', error));
        this.ga.setUserId(platform + ':' + uuid)
          .then(() => this.devService.addLog('GA setUserId success', platform + ':' + uuid))
          .catch(error => this.devService.addLog('GA setUserId fail', error));

        this.ga.enableUncaughtExceptionReporting(true);
      })
      .catch(error => this.devService.addLog('GA Error starting GoogleAnalytics', error));
  }

  regToken() {
    const platform = this.device.platform || 'Web';
    const uuid = this.device.uuid || 'faked_uuid';
    this.apiService.regToken(platform, uuid);
  }
}

