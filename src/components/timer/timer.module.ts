import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicPageModule } from 'ionic-angular';

import { TimerComponent } from './timer.component';

@NgModule({
  declarations: [ TimerComponent ],
  imports: [
    IonicPageModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  exports: [ TimerComponent ]
})
export class TimerModule {}
