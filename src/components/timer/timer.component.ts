import { Component, EventEmitter, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'timer',
  templateUrl: 'timer.component.html',
  animations: [
    trigger('timerState', [
      state('shown', style({ display: 'block', opacity: 1 })),
      state('hidden', style({ display: 'none', opacity: 0 })),
      transition('hidden => shown', [ animate('500ms', style({ opacity: 1 })) ]),
      transition('shown => hidden', [ animate('500ms', style({ opacity: 0 })) ])
    ])
  ]
})
export class TimerComponent {

  @Output() onTimerFinished  = new EventEmitter<void>();
  @Output() onClickClockFace = new EventEmitter<void>();

  // for timer logic
  timeoutId: any;
  timeInSeconds: number;
  remainSeconds: number;
  targetTime: Date;
  isStarted: boolean;
  isRunning: boolean;
  isFinished: boolean;


  displayTime: string;

  // for animation
  timerState = 'hidden';

  // config
  showButton = false;
  showHour   = true;

  initTimer(timeInSeconds: number): void {

    if (this.isRunning) { this.stopTimer(); }

    this.timeoutId = null;

    this.timeInSeconds = timeInSeconds;
    this.remainSeconds = this.timeInSeconds;
    this.targetTime = new Date((new Date()).setSeconds((new Date()).getSeconds() + timeInSeconds));

    this.isStarted = false;
    this.isRunning = false;
    this.isFinished = false;

    this.displayTime = this.getSecondsAsDigitalClock(this.remainSeconds);
  }

  startTimer(): void {
    this.isStarted = true;
    this.isRunning = true;
    this.timerTick();
  }

  pauseTimer(): void {
    this.isRunning = false;
    this.stopTimer();
  }

  resumeTimer(): void {
    this.isRunning = true;
    this.targetTime = new Date((new Date()).setSeconds((new Date()).getSeconds() + this.remainSeconds));
    this.timerTick();
  }

  stopTimer(): void {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
      this.timeoutId = null;
    }
  }

  timerTick(): void {
    this.timeoutId = setTimeout(() => {

      this.remainSeconds = this.getRemainSeconds();

      this.displayTime = this.getSecondsAsDigitalClock(this.remainSeconds);

      if (this.remainSeconds > 0) {
        this.timerTick();
      }
      else {
        this.isFinished = true;
        this.isRunning = false;
        this.onTimerFinished.emit();
      }
    }, 1000);
  }

  private getRemainSeconds(): number {
    const timeDiff = this.targetTime.getTime() - (new Date()).getTime(),
          remain = Math.ceil(timeDiff / 1000);
    return Math.max(remain, 0);
  }

  private getSecondsAsDigitalClock(inputSeconds: number): string {
    const sec_num = parseInt(inputSeconds.toString(), 10), // don't forget the second param
          hours   = Math.floor(sec_num / 3600),
          minutes = Math.floor((sec_num - (hours * 3600)) / 60),
          seconds = sec_num - (hours * 3600) - (minutes * 60),
          hoursString = (hours < 10) ? "0" + hours : hours.toString(),
          minutesString = (minutes < 10) ? "0" + minutes : minutes.toString(),
          secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();

    if (this.showHour) {
      return hoursString + ':' + minutesString + ':' + secondsString;
    }
    else {
      return minutesString + ':' + secondsString;
    }
  }

  showTimer(): void {
    this.timerState = 'shown';
  }

  hideTimer(): void {
    this.timerState = 'hidden';
  }
}
