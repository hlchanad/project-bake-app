import { Component } from '@angular/core';
import { ActionSheetController, IonicPage } from 'ionic-angular';

import { TranslateService } from '../../translate/translate.service';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  language: { display: string, code: string};

  constructor(private actionSheetCtrl: ActionSheetController,
              private translateService: TranslateService) {

    this.language = this.translateService.getCurrentLanguage();
  }

  onClickLanguage() {
    const supportedLanguages = this.translateService.getSupportedLanguages();

    const languageButtons = [];
    for(let i=0; i<supportedLanguages.length; i++) {
      const language = supportedLanguages[i];
      languageButtons.push({
        text: language.display,
        handler: () => {
          this.language = language;
          this.translateService.use(language.code);
        }
      });
    }
    languageButtons.push({
      text: this.translateService.translate('cancel'),
      role: 'cancel'
    });

    const actions = this.actionSheetCtrl.create({
      title: 'Choose your language',
      buttons: languageButtons
    });
    actions.present();
  }

}
