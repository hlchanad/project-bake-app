import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';


import { TranslateModule } from '../../translate/translate.module';
import { SettingsPage } from './settings';

@NgModule({
  declarations: [ SettingsPage ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    TranslateModule
  ],
  entryComponents: [ SettingsPage ]
})
export class SettingsPageModule {}
