import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Content, IonicPage, NavParams } from 'ionic-angular';

import { Recipe } from '../../models/recipe';
import { TimerComponent } from '../../components/timer/timer.component';
import { StepsComponent } from './components/steps/steps';
import { RecipeService } from '../../services/recipe';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { DevService } from '../../services/dev';

@IonicPage()
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html'
})
export class RecipePage implements OnInit {

  private stepsComponent: StepsComponent;

  @ViewChild(Content) content: Content;
  @ViewChild(TimerComponent) timerComponent: TimerComponent;
  @ViewChild('stepsComponent') set stepsChild(element: StepsComponent) {
    this.stepsComponent = element;
  };

  segment = 'overview';
  recipe: Recipe;
  index: number;

  shoppingListMode = false;

  @ViewChild('ionSegment') ionSegment: ElementRef;
  ionSegmentHeight: number;

  constructor(private navParams: NavParams,
              private ga: GoogleAnalytics,
              private recipeService: RecipeService,
              private devService: DevService) {

    this.recipe = this.navParams.get('recipe');
    this.index = this.navParams.get('index');

    const title = `Recipe - ${this.recipe.title}`,
          url = `recipes/${this.recipe._id}`;
    this.ga.trackView(title, url)
      .then(() => this.devService.addLog('trackView success', title))
      .catch(error => this.devService.addLog('trackView fail', error));
  }

  ngOnInit() {
    const height = window.getComputedStyle(this.ionSegment.nativeElement)['height'];
    this.ionSegmentHeight = +(height.substr(0, height.length - 2)); // remove px and convert to number type
  }

  toggleShoppingListMode() {
    this.shoppingListMode = !this.shoppingListMode;
    if (this.shoppingListMode) { // only scroll when it' on
      this.content.scrollToBottom();
    }
  }

  onStepScrollTo(yPos: number) {
    if (yPos == StepsComponent.SCROLL_TO_BOT) {
      this.content.scrollToBottom();
    } else {
      this.content.scrollTo(0, yPos);
    }
  }


  // FAB buttons - start

  onPlayPause() {
    if (this.stepsComponent) {
      this.stepsComponent.onPlayPause();
    }
  }

  onForward() {
    if (this.stepsComponent) {
      this.stepsComponent.onForward();
    }
  }

  onStop() {
    if (this.stepsComponent) {
      this.stepsComponent.onStop();
    }
  }

  isPlaying(): boolean {
    return this.stepsComponent && this.stepsComponent.isPlaying();
  }

  // FAB buttons + Finish button - end

  isRecipeFavorited() {
    return this.recipeService.isRecipeFavorited(this.recipe._id);
  }

  toggleFavoriteRecipe() {
    if (this.isRecipeFavorited()) {
      this.recipeService.removeFavoriteRecipe(this.recipe._id);
    }
    else {
      this.recipeService.favoriteRecipe(this.recipe._id);
    }
  }
}
