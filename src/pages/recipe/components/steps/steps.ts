import {
  Component, Input, OnInit, ViewChild, Output, EventEmitter, OnDestroy, ElementRef, AfterViewInit
} from '@angular/core';

import { AlertController, ToastController } from 'ionic-angular';

import { Recipe } from '../../../../models/recipe';
import { CurrentStep, RecipeStepHelperService, STATUS_PLAYING } from '../../../../services/recipe-step-helper';
import { TimerComponent } from '../../../../components/timer/timer.component';
import { ShoppingListService } from '../../../../services/shopping-list';
import { TranslateService } from '../../../../translate/translate.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { DevService } from '../../../../services/dev';
import { NativeAudio } from '@ionic-native/native-audio';

@Component({
  selector: 'app-recipe-steps',
  templateUrl: 'steps.html'
})
export class StepsComponent implements OnInit, OnDestroy, AfterViewInit {

  static SCROLL_TO_BOT = -1;

  @Input() recipe: Recipe;

  // --- for calculating how many px should be scroll ---
  @Input() ionSegmentHeight: number;
  @Input() scrollHeight: number;
  @Input() contentHeight: number;
  numberOfIonListHeader: number;
  ionListHeaderHeight: number;
  numberOfIonItem: number;
  ionItemHeight: number;
  ionListMarginBottom: number;
  // ----------------------------------------------------

  @ViewChild(TimerComponent) timerComponent: TimerComponent;

  @Output() scrollTo = new EventEmitter<number>(); // y-position

  finishedTimerSubscription: any;
  finishedRecipeSubscription: any;
  stepChangesSubscription: any;

  constructor(private toastCtrl: ToastController,
              private elementRef: ElementRef,
              private ga: GoogleAnalytics,
              private nativeAudio: NativeAudio,
              private alertCtrl: AlertController,
              private recipeStepHelper: RecipeStepHelperService,
              private shoppingListService: ShoppingListService,
              private translateService: TranslateService,
              private devService: DevService) {}

  ngOnInit() {
    // init RecipeStepHelper
    this.recipeStepHelper.setRecipe(this.recipe, this);

    // ref: http://www.orangefreesounds.com/loud-alarm-clock-sound/
    this.nativeAudio.preloadComplex('alarm', 'assets/ringtone/alarm.mp3', 1, 1, 0)
      .then((data) => this.devService.addLog('nativeAudio preloadComplex success', data))
      .catch((error) => this.devService.addLog('nativeAudio preloadComplex fail', error));

    // subscription from RecipeStepHelper
    this.subscribeFinishedTimer();
    this.subscribeFinishedRecipe();
    this.subscribeStepChanges();
  }

  ngAfterViewInit() {
    // calculation for scrolling
    this.calculateConstantsForScroll();
  }

  ngOnDestroy() {
    this.finishedTimerSubscription.unsubscribe();
    this.finishedRecipeSubscription.unsubscribe();
    this.stepChangesSubscription.unsubscribe();
  }

  // SUBSCRIPTIONS - start

  subscribeFinishedTimer() {
    this.finishedTimerSubscription = this.recipeStepHelper.finishedTimer
      .subscribe(() => {
        this.nativeAudio.loop('alarm')
          .then((data) => this.devService.addLog('nativeAudio loop success', data))
          .catch((error) => this.devService.addLog('nativeAudio loop fail', error));

        this.alertCtrl.create({
          title: this.translateService.translate('timesup'),
          buttons: [{
            text: this.translateService.translate('okay'),
            handler: () => {
              this.recipeStepHelper.next();

              if (!this.isTimerOngoing()) {
                this.timerComponent.hideTimer();
              }

              this.nativeAudio.stop('alarm')
                .then((data) => this.devService.addLog('nativeAudio stop success', data))
                .catch((error) => this.devService.addLog('nativeAudio stop fail', error));
            }
          }]
        }).present();
      });
  }

  subscribeFinishedRecipe() {
    this.finishedRecipeSubscription = this.recipeStepHelper.finishedRecipe
      .subscribe(() => {

        const category = 'recipe',
              action = 'finishRecipe';

        this.ga.trackEvent(category, action)
          .then(() => this.devService.addLog('trackEvent success', { category: category, action: action }))
          .catch(error => this.devService.addLog('trackEvent fail', error));

        const toast = this.toastCtrl.create({
          message: this.translateService.translate('finishRecipe'),
          duration: 2000
        });
        toast.present();
      }
    );
  }

  subscribeStepChanges() {
    this.stepChangesSubscription = this.recipeStepHelper.stepChanges
      .subscribe((data: { currentStep: CurrentStep, isClickStepDirectly: boolean } ) => {
        const step = this.recipe.parts[data.currentStep.part].steps[data.currentStep.step];
        if (step.autoNextStep) {
          this.scrollToTimer();
          this.setTimer(step.duration);
          this.onPlayPause(); // immediately pause the timer
        }
        else if (this.isFinishable()) {
          this.scrollToBottom();
        }
        else if (!data.isClickStepDirectly){
          this.scrollToStep(data.currentStep.part, data.currentStep.step);
        }
      }
    );
  }

  // SUBSCRIPTIONS - end

  /**
   * trigger when clicking ion-item
   *
   * @param partIndex
   * @param stepIndex
   */
  changeCurrentStep(partIndex: number, stepIndex: number) {
    const currentStep = this.recipeStepHelper.currentStep;
    if (currentStep && currentStep.part == partIndex && currentStep.step == stepIndex) {
      // clicked current step
      this.onStop();
    }
    else {
      // clicked other step
      this.timerComponent.stopTimer();
      this.timerComponent.hideTimer();
      this.recipeStepHelper.startFromStep(partIndex, stepIndex);
    }
  }

  isPreviousStep(partIndex: number, stepIndex: number): boolean {
    const currentStep = this.recipeStepHelper.currentStep;
    return  currentStep
      && (currentStep.part > partIndex || currentStep.part == partIndex
      && currentStep.step > stepIndex);
  }

  isCurrentStep(partIndex: number, stepIndex: number): boolean {
    const currentStep = this.recipeStepHelper.currentStep;
    return  currentStep && currentStep.part == partIndex && currentStep.step == stepIndex;
  }

  isFinishable(): boolean {
    const currentStep = this.recipeStepHelper.currentStep;
    return this.isPlaying()
      && currentStep.part == this.recipe.parts.length-1
      && currentStep.step == this.recipe.parts[this.recipe.parts.length-1].steps.length-1;
  }

  isPlaying(): boolean {
    return this.recipeStepHelper.getStatus() == STATUS_PLAYING;
  }

  isTimerOngoing(): boolean {
    return this.recipeStepHelper.timer && this.recipeStepHelper.timer.isOngoing();
  }

  // FAB buttons + Finish button - start

  onPlayPause() {
    // not yet started on any step -> start with step 0
    if (this.recipeStepHelper.currentStep == null) {
      this.recipeStepHelper.startFromStep(0, 0, false);
    }
    else {
      const currentStep = this.recipeStepHelper.currentStep,
            step = this.recipe.parts[currentStep.part].steps[currentStep.step];

      if (step.autoNextStep) {
        if (this.isPlaying()) { // playing -> pause
          this.timerComponent.pauseTimer();
          this.recipeStepHelper.pause();
        }
        else { // pausing -> play
          this.timerComponent.resumeTimer();
          this.recipeStepHelper.resume();
        }
      }
      this.recipeStepHelper.toggleStatus();
    }
  }

  onForward() {
    this.timerComponent.stopTimer();
    this.timerComponent.hideTimer();
    this.recipeStepHelper.next();
  }

  onStop() {
    this.recipeStepHelper.stop();
    this.timerComponent.stopTimer();
    this.timerComponent.hideTimer();
  }

  onFinish() {
    this.recipeStepHelper.finish();
    this.shoppingListService.finishRecipe(this.recipe._id);
  }

  // FAB buttons + Finish button - end

  private setTimer(timeInSeconds: number) {
    this.timerComponent.initTimer(timeInSeconds);
    this.timerComponent.startTimer();
    this.timerComponent.showTimer();
  }

  private calculateConstantsForScroll() {
    let height;

    const ionListHeaders = <Element[]> this.elementRef.nativeElement.getElementsByTagName('ion-list-header');
    this.numberOfIonListHeader = ionListHeaders.length;
    height = window.getComputedStyle(ionListHeaders[0])['height'];
    this.ionListHeaderHeight = +(height.substr(0, height.length - 2)); // remove px and convert to number type

    const ionItems = <Element[]> this.elementRef.nativeElement.getElementsByTagName('ion-item');
    this.numberOfIonItem = ionItems.length - 1;
    height =  window.getComputedStyle(ionItems[1])['height'];
    this.ionItemHeight = +(height.substr(0, height.length - 2)); // remove px and convert to number type

    const ionLists = <Element[]> this.elementRef.nativeElement.getElementsByTagName('ion-list');
    height = window.getComputedStyle(ionLists[0])['margin-bottom'];
    this.ionListMarginBottom = +(height.substr(0, height.length - 2)); // remove px and convert to number type
  }

  private scrollToStep(partIndex: number, ingredientIndex: number) {
    const ionCardElement = this.elementRef.nativeElement.getElementsByTagName('timer')[0].getElementsByTagName('ion-card')[0],
          ionCardElementStyles = window.getComputedStyle(ionCardElement);

    let timerComponentHeight: number = 0;
    if (ionCardElementStyles['height'] !== 'auto') {
      const ionCardHeight = +(ionCardElementStyles['height'].substr(0, ionCardElementStyles['height'].length - 2)),
            ionCardMargin = +(ionCardElementStyles['margin'].substr(0, ionCardElementStyles['margin'].length - 2));
      timerComponentHeight = ionCardHeight + ionCardMargin * 2;
    }

    let numberOfPartBeforeCurrentPart = 0,
        numberOfStepBeforeCurrentPart = 0;

    for (let i = 0; i < this.recipe.parts.length; i ++) {
      if (i === partIndex) { break ; } // find current part -> abort

      numberOfPartBeforeCurrentPart ++;
      numberOfStepBeforeCurrentPart += this.recipe.parts[i].steps.length;
    }

    let scrollY = 0;
    scrollY += this.ionSegmentHeight;
    // scrollY += timerComponentHeight; // built on iphone and dont need this on9hack ? only bug on browser ?
    scrollY += (this.ionListHeaderHeight + this.ionListMarginBottom) * numberOfPartBeforeCurrentPart;
    scrollY += this.ionItemHeight * numberOfStepBeforeCurrentPart;

    if (ingredientIndex <= 0) {

    }
    else if (ingredientIndex <= 1) {
      scrollY += this.ionListHeaderHeight;

    }
    else {
      scrollY += this.ionItemHeight * (ingredientIndex - 1) + this.ionListHeaderHeight;
    }

    this.scrollTo.emit(scrollY);
  }

  private scrollToBottom() {
    this.scrollTo.emit(StepsComponent.SCROLL_TO_BOT);
  }

  private scrollToTimer() {
    this.scrollTo.emit(this.ionSegmentHeight);
  }
}
