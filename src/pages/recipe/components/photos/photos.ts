import { Component, Input, OnInit } from '@angular/core';

import { ModalController } from 'ionic-angular';

import { GalleryModal } from 'ionic-gallery-modal';

import { Recipe } from '../../../../models/recipe';
import { Image } from '../../../../models/image';

@Component({
  selector: 'app-recipe-photos',
  templateUrl: 'photos.html'
})
export class PhotosComponent implements OnInit {

  static IMAGE_GRID_NUMBER_OF_COLUMNS = 3;

  @Input() recipe: Recipe;

  imageGrid: Image[][] = [];

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {
    console.log('ngOnInit recipe', this.recipe);
    this.setImageGrid();
  }

  onClickImage(image: Image): void {
    let photos = [];
    for (let i=0; i<this.recipe.images.length; i++) {
      photos.push({
        url: this.recipe.images[i].imageUrl
      });
    }

    const modal = this.modalCtrl.create(GalleryModal, {
      photos: photos,
      initialSlide: this.recipe.images.indexOf(image)
    });
    modal.present();
  }

  private setImageGrid(columns: number = PhotosComponent.IMAGE_GRID_NUMBER_OF_COLUMNS): void {

    let index = 0;

    for (let i=0; i<Math.ceil(this.recipe.images.length/columns); i++) {
      let imageGridRow: Image[] = [];

      for (let j=0; j<columns; j++) {
        if (this.recipe.images[index]) {
          imageGridRow.push(this.recipe.images[index++]);
        }
        else {
          imageGridRow.push({imageUrl: ''});
        }
      }
      this.imageGrid.push(imageGridRow);
    }
  }
}
