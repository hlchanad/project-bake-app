import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Recipe } from '../../../../models/recipe';
import { Ingredient } from '../../../../models/ingredient';
import { IngredientStatus } from '../../../../models/shopping-list-ingredient';
import { ObjectIdService } from '../../../../services/object-id';
import { ShoppingListService } from '../../../../services/shopping-list';
import { Checkbox, ToastController } from 'ionic-angular';
import { TranslateService } from '../../../../translate/translate.service';

@Component({
  selector: 'app-recipe-ingredients',
  templateUrl: 'ingredients.html'
})
export class IngredientsComponent implements OnChanges {

  @Input() recipe: Recipe;
  @Input() shoppingListMode = false;
  bulkBuyMode = false;
  bulkBuyList: {
    partIndex: number;
    ingredientIndex: number;
    ingredient: Ingredient;
    ingredient_zh: Ingredient
  }[] = [];

  constructor(private toastCtrl: ToastController,
              private translateService: TranslateService,
              private shoppingListService: ShoppingListService,
              private objectIdService: ObjectIdService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['shoppingListMode']['currentValue']) {
      this.onCancelBulkBuyMode();
    }
  }

  isIngredientBought(partIndex: number, ingredientIndex: number): boolean {
    return this.shoppingListService.isIngredientBought(this.recipe._id, partIndex, ingredientIndex);
  }

  canRecipeAddedToShoppingList() {
    return this.shoppingListService.canRecipeAddedToShoppingList(this.recipe._id);
  }

  onClickIngredient(partIndex: number, ingredientIndex: number, ingredient: Ingredient) {
    // not in shopping list mode
    // in shopping list mode but haven't added to shopping list
    // in shopping list mode but in bulk buy mode
    if (!this.shoppingListMode || this.canRecipeAddedToShoppingList() || this.bulkBuyMode)
      return ;

    const ingredient_zh = this.recipe.parts_zh[partIndex].ingredients[ingredientIndex];

    this.shoppingListService.boughtIngredient({
      _id: this.objectIdService.getObjectId('shopping-list'),
      name: ingredient.name,
      quantity: ingredient.quantity,
      unit: ingredient.unit,
      name_zh: ingredient_zh.name,
      quantity_zh: ingredient_zh.quantity,
      unit_zh: ingredient_zh.unit,
      status: IngredientStatus.Active,
      recipeRef: {
        recipeId: this.recipe._id,
        partIndex: partIndex,
        ingredientIndex: ingredientIndex
      }
    });
  }

  addAllToShoppingList() {
    for (let i=0; i<this.recipe.parts.length; i++) {
      for (let j=0; j<this.recipe.parts[i].ingredients.length; j++) {
        const ingredient    = this.recipe.parts[i].ingredients[j],
              ingredient_zh = this.recipe.parts_zh[i].ingredients[j];

        this.shoppingListService.addIngredient({
          _id: this.objectIdService.getObjectId('shopping-list'),
          name: ingredient.name,
          quantity: ingredient.quantity,
          unit: ingredient.unit,
          name_zh: ingredient_zh.name,
          quantity_zh: ingredient_zh.quantity,
          unit_zh: ingredient_zh.unit,
          status: IngredientStatus.Active,
          recipeRef: {
            recipeId: this.recipe._id,
            partIndex: i,
            ingredientIndex: j
          }
        });
      }
    }

    this.toastCtrl.create({
      message: this.translateService.translate('addedToShoppingList'),
      duration: 2000
    }).present();
  }

  haventBoughtAllYet(): boolean {
    return !this.shoppingListService.isAllIngredientsAreBoughtFromRecipe(this.recipe._id);
  }

  onTurnOnBulkBuyMode() {
    this.bulkBuyMode = true;
    this.bulkBuyList = [];
  }

  onCancelBulkBuyMode() {
    this.bulkBuyMode = false;
    this.bulkBuyList = [];
  }

  isBulkBuyMode(): boolean {
    return this.bulkBuyMode;
  }

  onClickBulkBuy() {
    this.bulkBuyList.forEach((el) => {
      this.shoppingListService.boughtIngredient({
        _id: this.objectIdService.getObjectId('shopping-list'),
        name: el.ingredient.name,
        quantity: el.ingredient.quantity,
        unit: el.ingredient.unit,
        name_zh: el.ingredient_zh.name,
        quantity_zh: el.ingredient_zh.quantity,
        unit_zh: el.ingredient_zh.unit,
        status: IngredientStatus.Active,
        recipeRef: {
          recipeId: this.recipe._id,
          partIndex: el.partIndex,
          ingredientIndex: el.ingredientIndex
        }
      });
    });

    this.onCancelBulkBuyMode();
  }

  onToggleIngredient(partIndex: number, ingredientIndex: number, ingredient: Ingredient, event: Checkbox) {
    const bulkBuyItem = {
      partIndex: partIndex,
      ingredientIndex: ingredientIndex,
      ingredient: ingredient,
      ingredient_zh: this.recipe.parts_zh[partIndex].ingredients[ingredientIndex]
    };

    if (event.checked) {
      this.bulkBuyList.push(bulkBuyItem);
    } else {
      const index = this.bulkBuyList.findIndex(
        (element) => element.partIndex === partIndex && element.ingredientIndex === ingredientIndex);
      this.bulkBuyList.splice(index, 1);
    }
  }
}
