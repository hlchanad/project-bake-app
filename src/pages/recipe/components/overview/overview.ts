import { Component, Input } from '@angular/core';

import { Recipe } from '../../../../models/recipe';

@Component({
  selector: 'app-recipe-overview',
  templateUrl: 'overview.html'
})
export class OverviewComponent {
  @Input() recipe: Recipe;

  constructor() {}
}
