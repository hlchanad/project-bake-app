import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicPageModule } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
import { LocalNotifications } from '@ionic-native/local-notifications';

// -------------------- Ionic Gallery Modal ------------------------
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import * as IonicGalleryModal from 'ionic-gallery-modal';
// ------------------------------------------------------------------

import { TranslateModule } from '../../translate/translate.module';
import { SharedModule } from '../../shared/shared.module';
import { TimerModule } from '../../components/timer/timer.module';

import { RecipePage } from './recipe';
import { StepsComponent } from './components/steps/steps';
import { PhotosComponent } from './components/photos/photos';
import { OverviewComponent } from './components/overview/overview';
import { IngredientsComponent } from './components/ingredients/ingredients';
import { RecipeStepHelperService } from '../../services/recipe-step-helper';

@NgModule({
  declarations: [
    RecipePage,
    OverviewComponent,
    IngredientsComponent,
    StepsComponent,
    PhotosComponent
  ],
  imports: [
    BrowserAnimationsModule,
    IonicPageModule.forChild(RecipePage),
    IonicGalleryModal.GalleryModalModule,
    TranslateModule,
    SharedModule,
    TimerModule
  ],
  entryComponents: [ RecipePage ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: IonicGalleryModal.GalleryModalHammerConfig },
    NativeAudio,
    LocalNotifications,
    RecipeStepHelperService
  ]
})
export class RecipePageModule {}
