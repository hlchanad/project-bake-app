import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ShoppingListIngredient } from '../../models/shopping-list-ingredient';
import { ShoppingListService } from '../../services/shopping-list';
import { Ingredient } from '../../models/ingredient';
import { RecipeService } from '../../services/recipe';
import { TranslateService } from '../../translate/translate.service';

@IonicPage()
@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage {

  ingredients: ShoppingListIngredient[] = [];

  reorganizedIngredients: {
    recipeId?: string,
    title: string,
    title_zh: string,
    ingredients: ShoppingListIngredient[]
  }[] = [];

  constructor(private shoppingListService: ShoppingListService,
              private recipeService: RecipeService) {

    this.shoppingListService.getIngredientsFromStorage()
      .then((ingredients: ShoppingListIngredient[]) => {
        this.ingredients = ingredients;
        this.reorganizedIngredients = this.reorganizeIngredients(this.ingredients);
      });
  }

  ionViewWillEnter() {
    this.fetchData();
  }

  onRemoveIngredient(ingredient: ShoppingListIngredient, index: number) {
    this.shoppingListService.boughtIngredient(ingredient);
    this.fetchData();
  }

  private reorganizeIngredients(ingredients: ShoppingListIngredient[]) {
    const recipeWise = {};
    const notInRecipe = [];

    ingredients.forEach((ingredient: ShoppingListIngredient) => {
      if (ingredient.recipeRef) {
        if (!recipeWise[ingredient.recipeRef.recipeId]) {
          recipeWise[ingredient.recipeRef.recipeId] = [];
        }
        recipeWise[ingredient.recipeRef.recipeId].push(ingredient);
      }
      else {
        notInRecipe.push(ingredient);
      }
    });

    const output = [];
    Object.keys(recipeWise).forEach((recipeId: string) => {
      const name = this.recipeService.getRecipeName((recipeId));
      output.push({
        recipeId: recipeId,
        title: name.title,
        title_zh: name.title_zh,
        ingredients: recipeWise[recipeId]
      });
    });

    if (notInRecipe.length > 0) {
      output.push({
        title: 'Other',
        title_zh: '其他',
        ingredients: notInRecipe
      });
    }

    return output;
  }

  private fetchData() {
    this.ingredients = this.shoppingListService.getIngredients();
    this.reorganizedIngredients = this.reorganizeIngredients(this.ingredients);
  }
}
