import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingListPage } from './shopping-list';
import { TranslateModule } from '../../translate/translate.module';

@NgModule({
  declarations: [ ShoppingListPage ],
  imports: [
    IonicPageModule.forChild(ShoppingListPage),
    TranslateModule
  ],
  entryComponents: [ ShoppingListPage ]
})
export class ShoppingListPageModule {}
