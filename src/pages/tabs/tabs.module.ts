import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { TranslateModule } from '../../translate/translate.module';

import { TabsPage } from './tabs';

@NgModule({
  declarations: [ TabsPage ],
  imports: [
    IonicPageModule.forChild(TabsPage),
    TranslateModule
  ],
  entryComponents: [ TabsPage ]
})
export class TabsPageModule {}
