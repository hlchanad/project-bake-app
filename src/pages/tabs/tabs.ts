import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';

import { RecipesPage } from '../recipes/recipes';
import { ShoppingListPage } from '../shopping-list/shopping-list';
import { FavoriteRecipesPage } from '../favorite-recipes/favorite-recipes';
import { SettingsPage } from '../settings/settings';
import { DevPage } from '../dev/dev';

import { DEV_MODE } from '../../config/config';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  recipesPage = RecipesPage;
  shoppingListPage = ShoppingListPage;
  favoriteRecipesPage = FavoriteRecipesPage;
  settingsPage = SettingsPage;
  devPage = DevPage;

  isDevMode(): boolean {
    return DEV_MODE;
  }
}
