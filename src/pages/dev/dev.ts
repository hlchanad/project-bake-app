import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { DevService } from '../../services/dev';

@IonicPage()
@Component({
  selector: 'page-dev',
  templateUrl: 'dev.html',
})
export class DevPage {

  constructor(private devService: DevService) {}

  getDevLogs() {
    return this.devService.getLogs();
  }

  onDiscardLogs() {
    this.devService.discardLogs();
  }
}
