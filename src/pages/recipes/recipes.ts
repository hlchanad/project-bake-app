import { Component, OnInit } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, Refresher } from 'ionic-angular';

import { Recipe } from '../../models/recipe';
import { RecipeService } from '../../services/recipe';
import { RecipePage } from '../recipe/recipe';

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage implements OnInit {

  recipes: Recipe[] = [];

  constructor(private navCtrl: NavController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private recipeService: RecipeService) {}

  ngOnInit() {
    const loading = this.loadingCtrl.create();
    loading.present();

    this.recipeService.getRecipes()
      .then((recipes: Recipe[]) => {
        this.recipes = recipes;

        this.recipeService.getRecipesFromServer()
          .then((recipes: Recipe[]) => {
            loading.dismiss();
            this.recipes = recipes;
          })
          .catch((error) => {
            loading.dismiss();
            const alert = this.alertCtrl.create({
              title: 'Error',
              message: 'Could not get data from server. Please check network connectivity.',
              buttons: ['Ok']
            });
            alert.present();

          });
      })
      .catch((error) => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Error',
          message: 'Could not get data from storage. Please check permissions.',
          buttons: ['Ok']
        });
        alert.present();
      });
  }

  pullToRefresh(refresher: Refresher) {
    this.recipeService.getRecipesFromServer()
      .then((recipes: Recipe[]) => {
        refresher.complete();
        this.recipes = recipes;
      })
      .catch((error) => {
        refresher.complete();
        const alert = this.alertCtrl.create({
          title: 'Error',
          message: 'Could not get data from server. Please check network connectivity.',
          buttons: ['Ok']
        });
        alert.present();
      });
  }

  onClickRecipe(recipe: Recipe, index: number) {
    this.navCtrl.push(RecipePage, { recipe: recipe, index: index });
  }
}
