import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { TranslateModule } from '../../translate/translate.module';

import { RecipeService } from '../../services/recipe';

import { RecipesPage } from './recipes';

@NgModule({
  declarations: [
    RecipesPage
  ],
  imports: [
    IonicPageModule.forChild(RecipesPage),
    TranslateModule
  ],
  entryComponents: [
    RecipesPage
  ],
  providers: [ RecipeService ]
})
export class RecipesPageModule {}
