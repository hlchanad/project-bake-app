import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { TranslateModule } from '../../translate/translate.module';

import { FavoriteRecipesPage } from './favorite-recipes';

@NgModule({
  declarations: [ FavoriteRecipesPage ],
  imports: [
    IonicPageModule.forChild(FavoriteRecipesPage),
    TranslateModule
  ],
  entryComponents: [ FavoriteRecipesPage ]
})
export class FavoriteRecipesPageModule {}
