import { Component } from '@angular/core';

import { IonicPage, NavController } from 'ionic-angular';

import { FavoriteRecipe } from '../../models/favorite-recipe';
import { RecipeService } from '../../services/recipe';
import { Recipe } from '../../models/recipe';
import { RecipePage } from '../recipe/recipe';

@IonicPage()
@Component({
  selector: 'page-favorite-recipes',
  templateUrl: 'favorite-recipes.html'
})
export class FavoriteRecipesPage {

  recipes: Recipe[] = [];

  constructor(private navCtrl: NavController,
              private recipeService: RecipeService) {}

  ionViewWillEnter() {
    this.getFavoriteRecipes();
  }

  getFavoriteRecipes() {
    this.recipeService.getFavoriteRecipes()
      .then((recipes: Recipe[]) => {
        this.recipes = recipes;
      })
      .catch();
  }

  onClickRecipe(recipe: Recipe, index: number) {
    this.navCtrl.push(RecipePage, { recipe: recipe, index: index });
  }

  onRemoveFavorite(recipe: Recipe) {
    this.recipeService.removeFavoriteRecipe(recipe._id);
    this.getFavoriteRecipes();
  }
}
