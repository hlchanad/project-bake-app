// export const HOST_URL = 'http://localhost:4080/project-bake';
// export const HOST_URL = 'https://nodejs-hlchanad.rhcloud.com/project-bake';
export const HOST_URL = 'https://project-bake-server.herokuapp.com/project-bake'

export const APP_VERSION = '0.1';

export const DEV_MODE = true;
